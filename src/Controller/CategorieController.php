<?php

namespace App\Controller;


use App\Entity\Categorie;
use App\Form\CategorieType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class CategorieController extends AbstractController
{
    /**
     * @Route("/categorie_nouvelle", name="nouvelle_categorie")
     */
      //Ajout d'un nouveau article
      public function nouvelle_categorie(Request $request): Response
      {
  
          $categorie = new Categorie();          
  
           $form = $this->createForm(CategorieType::class, $categorie);
   
           $form->handleRequest($request);
           if ($form->isSubmitted() && $form->isValid()) {
               
            $categorie = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categorie);
            $entityManager->flush();          
   
               return $this->redirectToRoute('home');
           }
           return $this->renderForm('categorie/addcategorie.html.twig', [
              'form' => $form,
          ]);
      }

       /**
     * @Route("/categorie_affiche", name="affiche_categorie")
     */

     //Affichage de toutes les categoris
     public function affiche_categorie(EntityManagerInterface $entityManager): Response
     {
        $categories = $entityManager->getRepository(Categorie::class)->findAll();
        return $this->render('categorie/categories.html.twig', [
            'categories' => $categories,
        ]);
     }
}
