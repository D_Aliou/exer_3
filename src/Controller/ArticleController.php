<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article_nouveau", name="nouveau_article")
     */

     //Ajout d'un nouveau article
    public function nouveau_article(Request $request): Response
    {

        $article = new Article();
        $article->setDateDeCreation(new \DateTime());

         $form = $this->createForm(ArticleType::class, $article);
 
         $form->handleRequest($request);
         if ($form->isSubmitted() && $form->isValid()) {
             
             $article = $form->getData(); 
             $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();          
             
 
             return $this->redirectToRoute('home');
         }
         return $this->renderForm('article/addarticle.html.twig', [
            'form' => $form,
        ]);
    }    

    /**
     * @Route("/article_affichage/{id}", name="affiche_article")
     */

     //Affichage d'un article
     public function affiche_article(Article $article): Response
     {
        return $this->render('article/articledetail.html.twig', [
            'article' => $article,
        ]);
     }

      /**
     * @Route("/article_supprime/delete/{id}", name="supprime_article")
     */

     //Affichage d'un article
     public function supprime_article(Article $article, EntityManagerInterface $entityManager): Response
     {
        $entityManager->remove($article);
        $entityManager->flush();
        return $this->redirectToRoute('home');
     }
}
