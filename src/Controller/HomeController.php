<?php

namespace App\Controller;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home_home")
     */
    public function index(): Response
    {
        $msg = "Bienvenue";
        return $this->render('home/index.html.twig', [
            'bienvenue' => $msg,
        ]);
    }

    /**
     * @Route("/", name="home")
     */

     //Affichage de tous les articles
     public function affichage_articles(EntityManagerInterface $entityManager): Response
     {
        $articles = $entityManager->getRepository(Article::class)->findAll();
        return $this->render('home/index.html.twig', [
            'articles' => $articles,
        ]);
     }
}
